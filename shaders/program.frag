#version 150
in vec3 vertColor;
in vec3 vertNormal;
in vec3 vertPosition;
in vec3 eyeVec;
in vec3 lightVec;
in vec3 spotDir;

out vec4 outColor;


uniform vec3 eyePos;

uniform sampler2D diffTex;
uniform sampler2D normTex;
uniform sampler2D heightTex;

uniform vec3 lightPos;
uniform float lightCutoff;
uniform float lightDist;

uniform int texSet;
uniform int isTex;
uniform int shaderSet;
uniform int parallaxSet;
uniform int isOcclusion;

void main() {
    if(shaderSet == 0) {
        vec3 matDifCol = vec3(0.8);
        vec3 matSpecCol = vec3(1);
        vec3 ambientLightCol = vec3(0.1);
        vec3 directLightCol = vec3(1, 0.9, 0.9);

        float scaleL = 0.04;
        float scaleK = -0.02;

        switch (parallaxSet) {
            case 0:
                scaleL = 0.02;
                scaleK = 0.0;
                break;
            case 1:
                scaleL = 0.04;
                scaleK = -0.02;
                break;
            case 2:
                scaleL = 0.09;
                scaleK = 0.0;
                break;
        }

        vec2 texCoord = vertColor.xy * vec2(1,-1) + vec2(0,1);

        float height = texture(heightTex, texCoord).r;
        float v = height * scaleL + scaleK;
        vec3 eye = normalize(eyeVec);
        vec2 offset = eye.xy * v;
        texCoord = texCoord + offset;

        if( isOcclusion == 1 && isTex == 1 &&(texCoord.x > 1 || texCoord.y > 1 || texCoord.x < 0 || texCoord.y < 0))
            discard;

        vec3 inNormal = normalize(vertNormal);
        if (isTex == 1)
            inNormal = texture(normTex, texCoord).xyz * 2 - 1;

        if (isTex == 1)
            matDifCol = texture(diffTex, texCoord).xyz * matDifCol;

        vec3 lVec = normalize(lightVec);

        vec3 ambiComponent = ambientLightCol * matDifCol;

        vec3 tmpLVec = lVec;
        if (isTex == 0)
            lVec = normalize(lightPos - vertPosition);
        float difCoef = pow(max(0, lVec.z), 0.7) * max(0, dot(inNormal, lVec));
        vec3 difComponent = directLightCol * matDifCol * difCoef;

        if (isTex == 0)
            lVec = -normalize(vertPosition - lightPos);
        vec3 reflected = reflect(-lVec, inNormal);

        lVec = tmpLVec;

        vec3 tmpEyeVec = eyeVec;
        if (isTex == 0)
            tmpEyeVec = eyePos - vertPosition;
        float specCoef = pow(max(0, lVec.z), 0.7) * pow(max(0,
            dot(normalize(tmpEyeVec), reflected)
        ), 70);
        vec3 specComponent = directLightCol * matSpecCol * specCoef;

        float spotEffect = max(dot(normalize(spotDir), -lVec), 0);
        float dist = 1 - 1 / (lightDist / length(lightVec));
        if (spotEffect >  lightCutoff ) {
            float blend = clamp((spotEffect -  lightCutoff) / (1 - lightCutoff), 0, 1);
            outColor.rgb = mix(ambiComponent, ambiComponent + max(dist * (difComponent + specComponent), 0), blend);
        } else {
            outColor = vec4(ambiComponent, 1);
        }
    }
	if(shaderSet == 1 || shaderSet == 2 || shaderSet == 3) {
	    outColor =  vec4(vertColor, 1);
	}
}
