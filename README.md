# PGRF3 Úloha 1
[GitLab repository](https://gitlab.com/goryllaz/pgrf3-uloha1)  
[Last version of build](https://gitlab.com/goryllaz/pgrf3-uloha1/uploads/aa3d431a72b067992e07010e037004a8/PGRF3_Uloha1_v1.0.0.jar)

## Dependencies
* JOGL 2.3.2
* Vavr.io 1.0.0  
 
## Control
__Camera angle:__ mouse  
__Camera forward:__ W  
__Camera backwards:__ S  
__Camera left:__ A 
__Camera right:__ D  
__Camera up:__ Shift  
__Camera down:__ Control  

