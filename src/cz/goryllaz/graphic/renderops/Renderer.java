package cz.goryllaz.graphic.renderops;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import cz.goryllaz.graphic.renderset.RendererSettings;
import cz.goryllaz.graphic.renderset.Shape;
import cz.goryllaz.graphic.renderset.Texture;
import cz.goryllaz.graphic.system.ControllerSystem;
import cz.goryllaz.graphic.renderutils.MeshGenerator;
import oglutils.*;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

import java.util.ArrayList;
import java.util.List;

public class Renderer implements GLEventListener {
    private final ControllerSystem controller;
    private final RendererSettings settings;

    private int width, height, polygonMode = GL2GL3.GL_FILL;
    private float time;

    private OGLBuffers grid;
    private List<OGLTexture> diffTex, normTex, heightTex;

    int shaderGrid, locMatGrid, locTimeGrid, locEyeGrid,
            locLightGrid, locLightDirGrid, locLightCutoffGrid, locLightDistGrid,
            locShapeSetGrid, locTexSetGrid, locLightSetGrid, locAnimSetGrid, locShaderSetGrid, locParallaxSet,
            locIsTexGrid, locIsOcclusion;

    Mat4 proj;

    private ShaderUtils ShaderUtils;

    public Renderer(ControllerSystem controller, RendererSettings settings) {
        this.controller = controller;
        this.settings = settings;
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL2GL3 gl = drawable.getGL().getGL2GL3();

        System.out.println("Init GL is " + gl.getClass().getName());
        System.out.println("OpenGL version " + gl.glGetString(GL2GL3.GL_VERSION));
        System.out.println("OpenGL vendor " + gl.glGetString(GL2GL3.GL_VENDOR));
        System.out
                .println("OpenGL renderer " + gl.glGetString(GL2GL3.GL_RENDERER));
        System.out.println("OpenGL extension "
                + gl.glGetString(GL2GL3.GL_EXTENSIONS));

        if (settings.getShape() == Shape.SPHERE) {
            generateSphere(gl);
        }

        controller.setCam(
                controller.getCam().withPosition(new Vec3D(5, 5, 2.5))
                .withAzimuth(Math.PI * 1.25)
                .withZenith(Math.PI * -0.125)
        );

        gl.glEnable(GL2GL3.GL_DEPTH_TEST);

    }

    void generateSphere(GL2GL3 gl) {
        shaderGrid = ShaderUtils.loadProgram(gl, "/program");

        createBuffers(gl);
        grid = MeshGenerator.generateGrid(gl, 50, 50, "inParamPos");

        createTexture(gl);

        locMatGrid = gl.glGetUniformLocation(shaderGrid, "mat");
        locLightGrid = gl.glGetUniformLocation(shaderGrid, "lightPos");
        locEyeGrid = gl.glGetUniformLocation(shaderGrid, "eyePos");
        locTimeGrid = gl.glGetUniformLocation(shaderGrid, "time");
        locLightDirGrid = gl.glGetUniformLocation(shaderGrid, "lightDir");
        locLightCutoffGrid = gl.glGetUniformLocation(shaderGrid, "lightCutoff");
        locLightDistGrid = gl.glGetUniformLocation(shaderGrid, "lightDist");
        locShapeSetGrid = gl.glGetUniformLocation(shaderGrid, "shapeSet");
        locTexSetGrid = gl.glGetUniformLocation(shaderGrid, "texSet");
        locLightSetGrid = gl.glGetUniformLocation(shaderGrid, "lightSet");
        locAnimSetGrid = gl.glGetUniformLocation(shaderGrid, "animSet");
        locShaderSetGrid = gl.glGetUniformLocation(shaderGrid, "shaderSet");
        locIsTexGrid = gl.glGetUniformLocation(shaderGrid, "isTex");
        locParallaxSet = gl.glGetUniformLocation(shaderGrid, "parallaxSet");
        locIsOcclusion = gl.glGetUniformLocation(shaderGrid, "isOcclusion");
    }

    void createBuffers(GL2GL3 gl) {
        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 3),
                new OGLBuffers.Attrib("inNormal", 3)
        };
    }

    void createTexture(GL2GL3 gl) {
        diffTex = new ArrayList<>();
        normTex = new ArrayList<>();
        heightTex = new ArrayList<>();
        for (Texture texture: Texture.values()) {
            String texTmp = "/textures/bricks";
            switch (texture) {
                case BRICKS:
                    texTmp = "/textures/bricks";
                    break;
                case ROCK:
                    texTmp = "/textures/rock";
                    break;
                case WALL:
                    texTmp = "/textures/wall2";
                    break;
            }
            diffTex.add(new OGLTexture2D(gl, texTmp + ".jpg"));
            normTex.add(new OGLTexture2D(gl, texTmp + "n.png"));
            heightTex.add(new OGLTexture2D(gl, texTmp + "h.png"));
        }
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {

    }

    @Override
    public void display(GLAutoDrawable drawable) {
        time += 0.01;

        GL2GL3 gl = drawable.getGL().getGL2GL3();

        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, polygonMode);

        gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        gl.glUseProgram(shaderGrid);
        gl.glUniformMatrix4fv(locMatGrid, 1, false,
                ToFloatArray.convert(controller.getCam().getViewMatrix().mul(proj)), 0);
        gl.glUniform3fv(locEyeGrid, 1, ToFloatArray.convert(controller.getCam().getEye()), 0);
        gl.glUniform1f(locTimeGrid, time);
        switch (settings.getLight()) {
            case STATIC:
                gl.glUniform3fv(locLightGrid, 1, ToFloatArray.convert(
                        new Vec3D(5, 5, 1.5)
                ), 0);
                gl.glUniform3fv(locLightDirGrid, 1, ToFloatArray.convert(
                        new Vec3D(-5, -5, 0)
                ), 0);
                break;
            case ANIMATION:
                gl.glUniform3fv(locLightGrid, 1, ToFloatArray.convert(
                        new Vec3D(5, 5, 0).mul(new Vec3D(Math.cos(time), Math.sin(time), 0))
                ), 0);
                gl.glUniform3fv(locLightDirGrid, 1, ToFloatArray.convert(
                        new Vec3D(-5, -5, 0).mul(new Vec3D(Math.cos(time), Math.sin(time), 0))
                ), 0);
                break;
            case CAMERA:
                gl.glUniform3fv(locLightGrid, 1, ToFloatArray.convert(
                        controller.getCam().getEye()
                ), 0);
                gl.glUniform3fv(locLightDirGrid, 1, ToFloatArray.convert(
                        controller.getCam().getEye().add(
                                controller.getCam().getViewVector().mul(settings.getLightDist())
                        )
                ), 0);
                break;
        }

        gl.glUniform1f(locLightDistGrid, settings.getLightDist());
        gl.glUniform1f(locLightCutoffGrid, settings.getLightCutoff());

        gl.glUniform1i(locShapeSetGrid, settings.getShape().ordinal());
        gl.glUniform1i(locTexSetGrid, settings.getTexture().ordinal());
        gl.glUniform1i(locLightGrid, settings.getLight().ordinal());
        gl.glUniform1i(locAnimSetGrid, settings.getAnimation().ordinal());
        gl.glUniform1i(locShaderSetGrid, settings.getShader().ordinal());
        gl.glUniform1i(locParallaxSet, settings.getParallax().ordinal());
        gl.glUniform1i(locIsTexGrid, settings.isTexture()? 1 : 0);
        gl.glUniform1i(locIsOcclusion, settings.isOcclusion()? 1 : 0);


        diffTex.get(settings.getTexture().ordinal()).bind(shaderGrid, "diffTex", 0);
        normTex.get(settings.getTexture().ordinal()).bind(shaderGrid, "normTex", 1);
        heightTex.get(settings.getTexture().ordinal()).bind(shaderGrid, "heightTex", 2);

        grid.draw(GL2GL3.GL_TRIANGLES, shaderGrid);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y,
                        int width, int height) {
        this.width = width;
        this.height = height;
        proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
    }
}
