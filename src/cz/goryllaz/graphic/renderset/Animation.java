package cz.goryllaz.graphic.renderset;

public enum Animation {
    OFF,
    BLOB,
    ROTATION
}
