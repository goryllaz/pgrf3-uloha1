package cz.goryllaz.graphic.renderset;

public enum Shader {
    PER_PIXEL,
    PER_VERTEX,
    POSITION,
    NORMAL
}
