package cz.goryllaz.graphic.renderset;

public enum Parallax {
    LEVEL1,
    LEVEL2,
    LEVEL3
}
