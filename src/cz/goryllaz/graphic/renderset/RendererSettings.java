package cz.goryllaz.graphic.renderset;

public class RendererSettings {
    private Shape shape = Shape.SPHERE;
    private Texture texture = Texture.BRICKS;
    private Light light = Light.STATIC;
    private Animation animation = Animation.OFF;
    private Shader shader = Shader.PER_PIXEL;
    private Parallax parallax = Parallax.LEVEL2;

    private float lightDist = 20;
    private boolean isTexture = true;
    private boolean isOcclusion = true;

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public float getLightCutoff() {
        switch (light) {
            case STATIC:
                return 0.8f;
            case ANIMATION:
                return 0.8f;
            case CAMERA:
                return 0.95f;
        }
        return 0.9f;
    }

    public float getLightDist() {
        return lightDist;
    }

    public void setLightDist(float lightDist) {
        this.lightDist = lightDist;
    }

    public Animation getAnimation() {
        return animation;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public Shader getShader() {
        return shader;
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }

    public Parallax getParallax() {
        return parallax;
    }

    public void setParallax(Parallax parallax) {
        this.parallax = parallax;
    }

    public boolean isTexture() {
        return isTexture;
    }

    public void setTexture(boolean texture) {
        isTexture = texture;
    }

    public boolean isOcclusion() {
        return isOcclusion;
    }

    public void setOcclusion(boolean occlusion) {
        isOcclusion = occlusion;
    }
}
