package cz.goryllaz.graphic.system;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import cz.goryllaz.graphic.renderset.RendererSettings;
import cz.goryllaz.graphic.renderops.Renderer;

import javax.swing.*;

public class AppSystem {
    public static final int FPS = 60;
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private final FPSAnimator animator;
    private final GLCanvas canvas;
    private final ControllerSystem controller;
    private final RendererSettings settings;

    public AppSystem() {
        controller = new ControllerSystem();
        controller.start();


        GLProfile profile = GLProfile.getMaximum(true);
        GLCapabilities capabilities = new GLCapabilities(profile);
        capabilities.setDepthBits(24);

        settings = new RendererSettings();

        canvas = new GLCanvas(capabilities);
        Renderer renderer = new Renderer(controller, settings);
        canvas.addGLEventListener(renderer);
        canvas.setSize(WIDTH, HEIGHT);

        animator = new FPSAnimator(canvas, FPS, true);
    }

    public void start() {
        SwingUtilities.invokeLater(() -> {
                    new WindowSystem().open(canvas, controller, settings,
                            () -> {
                                new Thread() {
                                    @Override
                                    public void run() {
                                        if (animator.isStarted()) animator.stop();
                                        System.exit(0);
                                    }
                                }.start();
                            }
                    );
                    animator.start();
                }
        );
    }
}
