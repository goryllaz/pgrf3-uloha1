package cz.goryllaz.graphic.system;

public interface Command {
    default Void execute(Void v) {
        execute();
        return null;
    }

    void execute();
}
