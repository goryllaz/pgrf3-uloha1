package cz.goryllaz.graphic.system;

import transforms.Camera;

import java.awt.event.*;
import java.util.Arrays;

public class ControllerSystem extends Thread implements MouseListener,
        MouseMotionListener, KeyListener {

    private int auxX, auxY, auxX2, auxY2;
    private final int[] pressedKeys = new int[3];
    private boolean isDragged = false;
    private Camera cam = new Camera();

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public synchronized void keyPressed(KeyEvent keyEvent) {
        for (int i = 0; i < pressedKeys.length; i++) {
            if (pressedKeys[i] == keyEvent.getKeyCode()) break;
            if (pressedKeys[i] == 0) {
                pressedKeys[i] = keyEvent.getKeyCode();
                break;
            }
        }
    }

    @Override
    public synchronized void keyReleased(KeyEvent keyEvent) {
        Arrays.fill(pressedKeys,0);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        auxX = mouseEvent.getX();
        auxY = mouseEvent.getY();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        auxX = 0;
        auxY = 0;
        auxX2 = 0;
        auxY2 = 0;
        isDragged = false;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        auxX2 = mouseEvent.getX();
        auxY2 = mouseEvent.getY();
        isDragged = true;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }

    public int[] getPressedKeys() {
        return pressedKeys;
    }

    public synchronized int getKey() {
        //System.out.println(pressedKeys[0]);
        return pressedKeys[0];
    }

    public synchronized int getKey(int index) {
        //System.out.println(pressedKeys[0] + " " + pressedKeys[1] + " " + pressedKeys[2]);
        return pressedKeys[index];
    }

    public int getAuxX() {
        return auxX;
    }

    public void setAuxX(final int auxX) {
        this.auxX = auxX;
    }

    public int getAuxY() {
        return auxY;
    }

    public void setAuxY(final int auxY) {
        this.auxY = auxY;
    }

    public int getAuxX2() {
        return auxX2;
    }

    public void setAuxX2(final int auxX2) {
        this.auxX2 = auxX2;
    }

    public int getAuxY2() {
        return auxY2;
    }

    public void setAuxY2(final int auxY2) {
        this.auxY2 = auxY2;
    }

    public Camera getCam() {
        return cam;
    }

    public void setCam(Camera cam) {
        this.cam = cam;
    }

    public void run() {
        while (true) {
            switch (getKey()) {
                case KeyEvent.VK_W:
                    cam = cam.forward(0.05);
                    break;
                case KeyEvent.VK_D:
                    cam = cam.right(0.05);
                    break;
                case KeyEvent.VK_S:
                    cam = cam.backward(0.05);
                    break;
                case KeyEvent.VK_A:
                    cam = cam.left(0.05);
                    break;
                case KeyEvent.VK_CONTROL:
                    cam = cam.down(0.025);
                    break;
                case KeyEvent.VK_SHIFT:
                    cam = cam.up(0.025);
                    break;
            }

            if ( ( getAuxX() != getAuxX2() || getAuxY() != getAuxY2() )
                    && getAuxX2() != 0 && getAuxX2() != 0 ) {
                cam = cam.addAzimuth((double) Math.PI * (getAuxX() - getAuxX2()) / AppSystem.WIDTH)
                        .addZenith((double) Math.PI * (getAuxY() - getAuxY2()) / AppSystem.WIDTH);
                setAuxX(getAuxX2());
                setAuxY(getAuxY2());
            }

            try {
                //Thread.sleep(17);
                Thread.sleep(3);
            }
            catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
